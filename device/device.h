#ifndef DEVICE_H
#define DEVICE_H

#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#define NOF_IDS (3)

typedef struct device {
	// Initialized by user
    char* srvAddr;  /*!< String of server address */
    int port;       /*!< Port number */
    uint32_t devId;            /*!< Device id */
    int intervalMS;       /*!< Send interval in milliseconds */
    int32_t msgIds[NOF_IDS];  /*!< Each device supports up NOF_IDS msgIds */
    // Initialized by device.c
    int sfd;              /*!< Socket fd */
     /*!< Send single message */
    int (*sendSingle)(struct device* this, int msgId, char* data, size_t dataSize);
    int (*start)(struct device* this); /*!< Start send thread */
    int (*stop)(struct device* this);  /*!< Stop send thread */
    pthread_t thread; /*!< Thread handle */
    bool run;         /*!< Thread signal */
} device_t;

/**
* @brief Initializes the device_t
* @param dev pointer to allocated device_t structure
*/
int DEV_Init(device_t* dev);

/**
* @brief Deinitializes the device_t
* @param dev pointer to allocated device_t structure
*/
int DEV_Deinit(device_t* dev);

#endif /*DEVICE_H*/
