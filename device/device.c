#include "device.h"
#include "crc.h"
#include <sys/socket.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#define SIZE_ACK (13)
#define POS_ACK (10)

/**
 * @brief Waits for ACK
 * @param msg
 * @param dataSize Size of buffer
 */
static int waitForACK(device_t *dev, int msgId)
{
	uint8_t buffer[SIZE_ACK]; // Only 13 bytes necessary but we use more to be sure..

	ssize_t res = read(dev->sfd, buffer, 13);

	if(res < 0){
		// We got an error here
		perror("Could not read ACK from server");
		return -1;
	}

	// Check checksum
	if(CRC_checkCRC16(buffer, SIZE_ACK) < 0){
		perror("waitForACK: Checksum wrong");
		return -1; // checksum was wrong --> Do something here.
	}
	if(buffer[POS_ACK]){
		// We received an ack
		return 1;
	}

	// No ACK received
	return 0;
}


/**
 * @brief Packs and sends single data
 * @param this pointer to device_t
 * @param msgId Message id
 * @param data Pointer to data to send
 * @param dataSize Size of data
 * @return Errorcode
 */
static int sendSingle(device_t* this, int msgId, char* data, size_t dataSize)
{
    uint16_t crc;
    uint16_t nofBytes = 0;
    nofBytes += sizeof(this->devId);
    nofBytes += sizeof(msgId);
    nofBytes += dataSize;
    nofBytes += sizeof(crc);

    // Allocate send buffer including all fields
    uint8_t* sendBuffer = malloc(nofBytes + sizeof(nofBytes));
    if(sendBuffer == NULL){
        perror("sendSingle: Could not allocate sendBuffer");
        return -1;
    }

    uint8_t* ptr = sendBuffer;

    // Fill send Buffer
    memcpy(ptr, &nofBytes, sizeof(nofBytes));
    ptr += sizeof(nofBytes);

    memcpy(ptr, &this->devId, sizeof(this->devId));
    ptr += sizeof(this->devId);

    memcpy(ptr, &msgId, sizeof(msgId));
    ptr += sizeof(msgId);
    
    memcpy(ptr, data, dataSize);
    ptr += dataSize;

    crc = CRC_CRC16(sendBuffer, nofBytes);
    *ptr = (crc&0xFF00)>>8;
    *(ptr+1) = crc&0x00FF;

    // Send data
    write(this->sfd, sendBuffer, nofBytes + sizeof(nofBytes));

	int ret = waitForACK(this, msgId);
    
    // Free the data
    free(sendBuffer);
    
    return ret;
}


/**
 * @brief Task to send random messages to server
 * @param ptr pointer to run flag
 */
static void * send_task(void *ptr)
{
    char * dummydata = "Hello World!";
    int dummyId;
    // No sync flag is provided
    if (ptr == NULL) pthread_exit(NULL);
    device_t *dev = (device_t*) ptr;

    printf("Thread has started\n");

    while(dev->run){
        int r = rand();
        // Set random id
        dummyId = r % NOF_IDS;
        for(int tries = 0; tries<2; tries++){
            if(sendSingle(dev, dummyId, dummydata, sizeof("Hello World!"))){
                break;
            }
            // Wait 5ms
            usleep(5000);
        }
        usleep(dev->intervalMS*1000);
    }
    return NULL;
}


/**
 * @brief Creates and starts send thread
 * @param this Pointer to initialized device_t
 * @return Errorcode
 */
static int start(device_t* this)
{
    if (this == NULL) return -1;
    this->run = true;
    // Create thread
    pthread_create(&this->thread, NULL, send_task, (void *)this);
    return 0;
}


/**
 * @brief Stops and deletes send thread
 * @param this Pointer to initialized device_t
 * @return Errorcode
 */
static int stop(device_t* this)
{
    if (this == NULL) return -1;

    // Signal thread
    this->run = false;
    // Join thread
    pthread_join(this->thread, NULL);
    printf("Thread has joined\n");

    // Cancele thread
    return 0;
}


int DEV_Init(device_t* dev)
{
    if (dev == NULL) return -1;

    // Seed random
    srand(time(NULL));

    // Open TCP/IP connection
    struct sockaddr_in servaddr;

    dev->sfd = -1;

    // socket create and verification 
    dev->sfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (dev->sfd == -1) { 
        perror("Init: Could not create socket"); 
        return -1;
    } 
    memset(&servaddr, '\0', sizeof(servaddr)); 

    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = inet_addr(dev->srvAddr); 
    servaddr.sin_port = htons(dev->port); 

    // connect the client socket to server socket 
    if (connect(dev->sfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0) { 
        // Close fd
        close(dev->sfd);
        perror("Init: Could not establish connection to server"); 
        return -1; 
    } 

    // Fill device functions
    dev->sendSingle = &sendSingle;
    dev->start = &start;
    dev->stop = &stop;
    dev->run = 0;


    return 0;
}


int DEV_Deinit(device_t* dev)
{
    if (dev == NULL) {
        return -1;
    }

    if(dev->run){
        //Stop and exit running thread
        if(dev->stop(dev) == -1){
            perror("Deinit: Could not stop send task");
        }

    }
    // Close TCP/IP connection
    if(close(dev->sfd) == -1){
        perror("Deinit: Could not close socket");
        return -1;

    }
    return 0;
}
