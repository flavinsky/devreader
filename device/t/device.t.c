#include "wvtest.h"
#include "device.h"
#include "crc.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

#define PORT (40000)
#define DEV_ID (42)
#define MSG_ID (32)
#define ACK (1)


/**
 * @brief Task to mock server for testing
 * @param ptr pointer to run flag
 */
static void * srv_task(void *ptr)
{
	// No sync flag is provided
	if (ptr == NULL) pthread_exit(NULL);
	int *run = (int*) ptr;

	// Listen for connections
	// Create Socket
	struct sockaddr_in cli_addr;
	socklen_t cli_addr_len = sizeof(cli_addr);
	int fd;

	fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd < 0){
		perror("Could not open socket");
		pthread_exit(NULL);
	}

	// Create socket parameter
	struct sockaddr_in saddr;
	memset(&saddr, 0, sizeof(saddr));
	saddr.sin_family      = AF_INET;              // IPv4
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);    // Bind to all available interfaces
	saddr.sin_port        = htons(PORT);          // Requested port
	// Bind socket
	if(bind(fd, (struct sockaddr *) &saddr, sizeof(saddr))<0){
		perror("Could not bind socket");
		pthread_exit(NULL);
	}

	// Try to listen on socket
	if(listen(fd, SOMAXCONN)<0){
		perror("Could not listen on port");
		pthread_exit(NULL);
	}

	*run = 1;
	int cfd = accept(fd, (struct sockaddr *) & cli_addr, &cli_addr_len);
	if(cfd<0){
		perror("Error during client accepting");
		pthread_exit(NULL);
	}

	int rcvd = 0;
	char buffer[1024];
	uint16_t nofBytes = 0;
	uint16_t crc = 0;
	char ACK_DATA[13] = {11, 0, DEV_ID, 0, 0, 0, MSG_ID, 0, 0, 0, ACK, 0, 0};
	while(*run){
		ssize_t res = read(cfd, buffer, 2);
		if(res < 0){
			// We got an error here
			perror("Could not read from client");
			pthread_exit(NULL);
		}
		memcpy(&nofBytes, buffer, sizeof(nofBytes));

		// Receive remaining
		res = read(cfd, buffer+2, nofBytes);
		if(res < 0){
			// We got an error here
			perror("Could not read from client");
			pthread_exit(NULL);
		}

		// Check CRC
		if(CRC_checkCRC16(buffer, nofBytes+2) < 0){
			// Send back NACK!
			ACK_DATA[10] = 0;			
		}
		
		crc = CRC_CRC16(ACK_DATA, 11);
        ACK_DATA[11] = (crc&0xFF00)>>8;
        ACK_DATA[12] = crc&0x00FF;
		write(cfd, ACK_DATA, 13);

	}
	close(cfd);
	close(fd);
	return NULL;
}

WVTEST_MAIN("device ut")
{
	// Init CRC module
	CRC_Init();

	device_t mydev;
	mydev.srvAddr = "127.0.0.1";
	mydev.port = PORT;
	mydev.devId = DEV_ID;
	mydev.intervalMS = 2000;
	memset(mydev.msgIds, 2, sizeof(mydev.msgIds));

	WVPASS(DEV_Init(&mydev)<0);

	// Start Mockserver
	int run = 0;
	pthread_t thread;
	pthread_create(&thread, NULL, srv_task, (void *)&run);
	while(!run);

	WVPASS(DEV_Init(&mydev) == 0);
	WVPASS(mydev.sendSingle(&mydev, MSG_ID, "Hello you", sizeof("Hello you")) == 1);
    WVPASS(DEV_Deinit(&mydev) == 0);

	// Stop Mockserver
	run = 0;
	pthread_join(thread, NULL);
}
