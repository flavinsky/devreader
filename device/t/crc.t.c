#include "wvtest.h"
#include "crc.h"
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

#define DEV_ID (42)
#define MSG_ID (32)
#define ACK (1)

WVTEST_MAIN("crc ut")
{
    char testData1[5] = {0x00, 0x01, 0x02, 0x03, 0xff};
    char testData2[7] = {0x04, 0x03, 0x02, 0x03, 0xee, 0x24, 0x05};
    char testData3[7] = {0x04, 0x03, 0x02, 0x03, 0xee, 0xD0, 0xCC};

	// Init CRC module
	CRC_Init();
    
    WVPASS(CRC_CRC16(testData1, sizeof(testData1)) == 0x427b);
    WVPASS(CRC_checkCRC16(testData2, sizeof(testData2)) == 0);
    WVPASS(CRC_checkCRC16(testData3, sizeof(testData3)) < 0);

    char testData4[7] = {0x04, 0x00, 0x02, 0x03, 0xee, 0x00, 0x00};
    uint16_t crc = 0;
    crc = CRC_CRC16(testData4, 5);

    testData4[5] = (crc&0xFF00)>>8;
    testData4[6] = crc&0x00FF;
    WVPASS(CRC_checkCRC16(testData4, sizeof(testData4)) == 0);

	char ACK_DATA[13] = {0x0C, 0x00, 0x2a, 0x00, 0x00, 0x00, 0x2a, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00};
    crc = CRC_CRC16(ACK_DATA, 11);
    ACK_DATA[11] = (crc&0xFF00)>>8;
    ACK_DATA[12] = crc&0x00FF;
    WVPASS(CRC_checkCRC16(ACK_DATA, sizeof(ACK_DATA)) == 0);
}
