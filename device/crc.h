#ifndef CRC_H
#define CRC_H

#include <stdint.h>
#include <stddef.h>

/**
* @brief Checks CRC checksum
* @param data Pointer to data to check
* @param size Size of data
* @return 0 if checksum was ok, -1 otherwise
*/
int CRC_checkCRC16(const uint8_t* data, size_t size);

/**
* @brief Calculates CRC checksum
* @param data Pointer to data to check
* @param size Size of data
* @return CRC16
*/
uint16_t CRC_CRC16(const uint8_t* data, size_t size);

/**
* @brief Initializes table for CRC16 calculation
*        May be called mulitple times but funciton is not thread safe!
*/
void CRC_Init(void);


#endif /*CRC_H*/
