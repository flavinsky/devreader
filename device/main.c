#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <signal.h>
#include "device.h"
#include "crc.h"

void sighandler(int arg)
{
	exit(0); /* Invoke exit handlers */
}

void usage(const char* argv0)
{
	fprintf(stderr, "Usage: %s [options]\n", argv0);
	fprintf(stderr,
			"Options:\n"
			"  -d Delay between messages\n"
			"  -p Port\n"
			"  -a Server IP\n"
			"  -i Device ID\n"
		   );
}

int main(int argc, char** argv)
{
	int opt;
	int delayMS = 1000;
	int port = 40000;
    int id = 42;
	char* srv_addr = "127.0.0.1";
	while ((opt = getopt(argc, argv, "d:p:a:i:")) != -1) {
		switch (opt) {
			case 'd': delayMS = atoi(optarg); break;
			case 'p': port = atoi(optarg); break;
			case 'a': srv_addr = optarg; break;
            case 'i': id = atoi(optarg); break;
			default: /* '?' */
					  usage(argv[0]);
					  exit(1);
		}
	}
	signal(SIGINT, sighandler);
	signal(SIGTERM, sighandler);
	signal(SIGHUP, sighandler);
    
    // Init CRC table
    CRC_Init();


	device_t mydev;
	mydev.srvAddr = srv_addr;;
	mydev.port = port;
	mydev.devId = id;
	mydev.intervalMS = delayMS;
	memset(mydev.msgIds, 2, sizeof(mydev.msgIds));

	if(DEV_Init(&mydev)<0) return -1;

	mydev.start(&mydev);

	printf("Started device. Press any key to shutdown...");
	getchar();

	mydev.stop(&mydev);
    DEV_Deinit(&mydev);

	return 0;

}

