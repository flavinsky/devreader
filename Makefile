all: server device

build:
	mkdir $@

device: build
	$(MAKE) -C device
	cp device/device build/.


build/server.cmake: build
	cd $(@D) && cmake ../server

server: build/server.cmake
	$(MAKE) -C build

.PHONY: clean

clean:
	$(MAKE) -C build clean
	$(MAKE) -C device clean
	rm -f build/device
