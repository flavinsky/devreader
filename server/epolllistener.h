#ifndef EPOLLLISTENER_H
#define EPOLLLISTENER_H

#include "epollfd.h"
#include "devicedata.h"

class EpollListener : public EpollFd
{
public:
    EpollListener(DeviceData &data, short int port, EpollInstance &e);
    ~EpollListener();
    void handleEvent(uint32_t events);
private:
    DeviceData &data;
};

#endif // EPOLLLISTENER_H
