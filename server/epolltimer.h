#ifndef EPOLLTIMERIN_H
#define EPOLLTIMERIN_H

#include "epollfd.h"
#include "devicedata.h"

class EpollTimer : public EpollFd
{
public:
    EpollTimer(DeviceData &data, uint32_t timeMs, EpollInstance &e);
    ~EpollTimer();
    void handleEvent(uint32_t events);
private:
    DeviceData &data;
};

#endif // EPOLLTIMERIN_H
