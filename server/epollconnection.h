#ifndef EPOLLCONNECTION_H
#define EPOLLCONNECTION_H

#include <cstring>
#include <iostream>
#include <stdint.h>
#include "epollfd.h"
#include "devicedata.h"


class EpollConnection : public EpollFd
{
public:
    #define PARSBUF_SIZE (256)
    EpollConnection(DeviceData &data, int fd, EpollInstance &e);
    ~EpollConnection();
    void handleEvent(uint32_t events);
private:
    uint8_t pars_buf[PARSBUF_SIZE];
    int nofElem;
    DeviceData &data;
};

#endif // EPOLLCONNECTION_H
