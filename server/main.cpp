#include <iostream>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <unistd.h>
#include <getopt.h>
#include "epollinstance.h"
#include "epolltimer.h"
#include "epollstdin.h"
#include "epolllistener.h"
#include "crc.h"

using namespace std;

void usage(const char* argv0)
{
	fprintf(stderr, "Usage: %s [options]\n", argv0);
	fprintf(stderr,
			"Options:\n"
			"  -p Port\n"
		   );
}


int main(int argc, char *argv[])
{
	int opt;
	int port = 40000;
	while ((opt = getopt(argc, argv, "d:p:a:")) != -1) {
		switch (opt) {
			case 'p': port = atoi(optarg); break;
			default: /* '?' */
					  usage(argv[0]);
					  exit(1);
		}
	}

    // Init CRC module
    CRC_Init();

    // Create epoll instance
    EpollInstance ep;

    DeviceData data;

    // Registering Stdin
    EpollStdIn sin(data, ep);

    // Register TCP listener
    EpollListener lis1(data, port, ep);

    while(1){
        ep.waitAndHandleEvents();
    }

    return 0;
}
