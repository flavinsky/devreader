#include "crc.h"
//! Size of the tables which contain precalculated values
#define CRC_TABLE_SIZE 256

//! Generator polynomial for CRC-16
#define POLY_16 0x1021

// Table which holds precalculated CRC-16 values
static uint16_t sTable16[CRC_TABLE_SIZE];


static void calcTable16(void)
{
    // Temporary CRC-16 value
    uint16_t crc;

    for (uint16_t i = 0; i < CRC_TABLE_SIZE; i++)
    {
        crc = (uint16_t)(i << 8);

        uint8_t j = 8;
        while(j--)
        {
            // Calculate CRC-16 value
            if (crc & 0x8000)
            {
                crc = (uint16_t)((crc << 1) ^ POLY_16);
            }
            else
            {
                crc = (uint16_t)(crc << 1);
            }
        }
        // Store value into table
        sTable16[i] = crc;
    }

}

int CRC_checkCRC16(const uint8_t* data, size_t size)
{

	if (CRC_CRC16(data, size) == 0)  {
		return 0;
	} else {
		return -1; //crc wrong
	}
}


uint16_t CRC_CRC16(const uint8_t* data, size_t size)
{
    // Set initial checksum value
    uint16_t crc = 0xFFFF;//; 0x0000
    
    // Step trough the single bytes
    for (uint32_t i = 0; i < size; ++i)
    {
        // Calculate the CRC-16 with the precalculated values from the table
        crc = (uint16_t)(sTable16[data[i] ^ (crc >> 8)] ^ (crc << 8));
    }

    return crc;
}

void CRC_Init(void){
  calcTable16();
}
