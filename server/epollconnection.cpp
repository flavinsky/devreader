#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
#include <sstream>
#include <cstdlib>

#include "epollconnection.h"
#include "devicedata.h"
#include "crc.h"

EpollConnection::EpollConnection(DeviceData &data, int fd, EpollInstance &e) : EpollFd(fd, e), data(data)
{
    nofElem = 0;
    memset(this->pars_buf, 0, PARSBUF_SIZE);
    // Set socket to non-blocking operation
    int flags = fcntl(fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(fd, F_SETFL, flags);

    //Register connection to epoll
    registerFd(EPOLLIN | EPOLLET);
}

EpollConnection::~EpollConnection()
{
    unregisterFd();
}

void EpollConnection::handleEvent(uint32_t events)
{
#define BUF_SIZE 128
#define DEV_ID (42)
#define MSG_ID (32)
#define ACK (1)
#define POS_DEVID (2)
#define POS_MSGID (6)

    uint8_t read_buf[BUF_SIZE];
    uint8_t ACK_DATA[13] = {11, 0, DEV_ID, 0, 0, 0, MSG_ID, 0, 0, 0, ACK, 0, 0};

    int count = 0;
    if((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)){
        unregisterFd();
        close(fd);
    }else{

        // Receive Data
        while(true){
            count = read(fd, &read_buf, sizeof(read_buf)-1);
            if(count <= 0){
                //Break receive loop since error has occured
                break;
            }

            // Copy data to persistant buffer
            if(count+this->nofElem<PARSBUF_SIZE){
                memcpy(this->pars_buf, read_buf, count);
                this->nofElem+=count;
            }else{
                break;
            }
        };

        if(count == 0){
            //Connection closed?
            std::cout << "Client disconnected" << std::endl;
            unregisterFd();
            close(fd);
        } else if (errno == EAGAIN){

            // Get length 
            uint16_t nofBytes;
            memcpy(&nofBytes, this->pars_buf, sizeof(nofBytes));


            if(nofBytes > this->nofElem) return;

            // Check CRC
            if(CRC_checkCRC16(this->pars_buf, nofBytes+2) < 0){
                // Fill NACK!
                ACK_DATA[10] = 0;
            }else{
                int devId;
                int msgId;
                memcpy(&devId,&this->pars_buf[POS_DEVID], sizeof(devId));
                memcpy(&msgId,&this->pars_buf[POS_MSGID], sizeof(msgId));

                // Fill ACK
                ACK_DATA[10] = 1;
                memcpy(&ACK_DATA[POS_DEVID], &devId, sizeof(devId));
                memcpy(&ACK_DATA[POS_MSGID], &msgId, sizeof(msgId));
                this->data.pushData(devId);
            }



            uint16_t crc = CRC_CRC16(ACK_DATA, 11);
            ACK_DATA[11] = (crc&0xFF00)>>8;
            ACK_DATA[12] = crc&0x00FF;
            write(fd, ACK_DATA, 13);
            
            // Reset parsbuff
            this->nofElem = 0;

        }

    }
}
