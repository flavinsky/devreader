#include "devicedata.h"
#include <vector>

DeviceData::DeviceData()
{
}

DeviceData::~DeviceData()
{
}

int DeviceData::pushData(int devId)
{
	// Printing the vector 
	for (auto it = this->devData.begin(); it != this->devData.end(); ++it){
		if(it->deviceId == devId){
			// Everything fine increment counter
			it->count++;
			return 0;
		}
	}
	entry_t newEntry;
	newEntry.deviceId = devId;
	newEntry.count = 1;
	this->devData.push_back(newEntry);
	return 0;
}

int DeviceData::clearData(void)
{
	this->devData.clear();
	return 0;
}

std::vector<entry_t> DeviceData::getDataCopy(void)
{
	return this->devData;
}
