#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include "epolllistener.h"
#include "epollconnection.h"
#include "devicedata.h"

EpollListener::EpollListener(DeviceData &data, short int port, EpollInstance &e) : EpollFd(-1, e), data(data)
{
    // Create Socket
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0){
        perror("Could not open socket");
        exit(1);
    }

    // Create socket parameter
    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family      = AF_INET;              // IPv4
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);    // Bind to all available interfaces
    saddr.sin_port        = htons(port);          // Requested port
    // Bind socket
    if(bind(fd, (struct sockaddr *) &saddr, sizeof(saddr))<0){
        perror("Could not bind socket");
        exit(1);
    }

    // Try to listen on socket
    if(listen(fd, SOMAXCONN)<0){
        perror("Could not listen on port");
    }

    // Set socket to non blocking
    int flags = fcntl(fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(fd, F_SETFL, flags);

    registerFd(EPOLLIN|EPOLLET);
}

EpollListener::~EpollListener()
{
    unregisterFd();
}

void EpollListener::handleEvent(uint32_t events)
{
    sockaddr_in cli_addr;
    socklen_t cli_addr_len = sizeof(cli_addr);

    if((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)){
        unregisterFd();
    }else{
        int cfd = accept(fd,
                         (struct sockaddr *) & cli_addr,
                         &cli_addr_len);
        if(cfd<0){
            perror("Error during client accepting");
            exit(1);
        }
        new EpollConnection(this->data, cfd, epollInstance);
        std::cout << "Added new client" << inet_ntoa(cli_addr.sin_addr) << std::endl;
    }
}
