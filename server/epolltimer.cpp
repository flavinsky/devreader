#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <stdexcept>
#include <iomanip>
#include "epolltimer.h"
#include "devicedata.h"

EpollTimer::EpollTimer(DeviceData &data, uint32_t timeMs, EpollInstance &e) : EpollFd(-1, e), data(data)
{
    struct itimerspec its;
    memset(&its, 0, sizeof(its));

    fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
    if(fd == -1){
        throw std::runtime_error(std::string("timerfd_create: ") + std::strerror(errno));
    }

    its.it_interval.tv_sec = timeMs / 1000;
    its.it_interval.tv_nsec = (timeMs % 1000) * 1000000;
    its.it_value.tv_sec = timeMs / 1000;
    its.it_value.tv_nsec = (timeMs % 1000) * 1000000;

    if(timerfd_settime(fd, 0, &its, NULL)){
        throw std::runtime_error(std::string("timerfd_settime: ") + std::strerror(errno));
    }

    registerFd(EPOLLIN);
}

EpollTimer::~EpollTimer()
{
    unregisterFd();
}

void EpollTimer::handleEvent(uint32_t events)
{
    uint64_t value;
    if((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)){
        unregisterFd();
    }else{
        read(fd, &value, 8);
        std::cout << "Current statistics" << std::endl;
        std::cout << "----------------------------------------------" << std::endl;

        // Get data
        std::vector<entry_t> devData = this->data.getDataCopy();
        std::cout << "    " << std::setw(10) << "DevId"  << ": " << std::setw(10) << "Count" << std::endl;
        for (auto it = devData.begin(); it != devData.end(); ++it){
            std::cout << "Dev " << std::setw(10) << it->deviceId << ": " << std::setw(10) << it->count << std::endl;
        }
    }
}
