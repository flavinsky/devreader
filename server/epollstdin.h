#ifndef EPOLLSTDIN_H
#define EPOLLSTDIN_H

#include <sys/epoll.h>
#include "epolltimer.h"
#include "epollfd.h"
#include "epollinstance.h"
#include "devicedata.h"

class EpollStdIn : public EpollFd
{
public:
    EpollStdIn(DeviceData &data, EpollInstance &e);
    ~EpollStdIn();
    void handleEvent(uint32_t events);
private:
    DeviceData &data;
    EpollTimer * autoStatTimer;
};
#endif // EPOLLSTDIN_H
