#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include "epollstdin.h"
#include "devicedata.h"

EpollStdIn::EpollStdIn(DeviceData &data, EpollInstance &e) : EpollFd(STDIN_FILENO, e), data(data)
{
    this->autoStatTimer = NULL;
    registerFd(EPOLLIN);
}

EpollStdIn::~EpollStdIn()
{
    unregisterFd();
}

void EpollStdIn::handleEvent(uint32_t events)
{
    std::string line;
    if((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)){
        unregisterFd();
    }else{
        std::getline(std::cin, line);
        if((line.compare("print stats")) == 0){
            std::cout << "Current statistics" << std::endl;
            std::cout << "----------------------------------------------" << std::endl;

            // Get data
            std::vector<entry_t> devData = this->data.getDataCopy();
            std::cout << "    " << std::setw(10) << "DevId"  << ": " << std::setw(10) << "Count" << std::endl;
            for (auto it = devData.begin(); it != devData.end(); ++it){
                std::cout << "Dev " << std::setw(10) << it->deviceId << ": " << std::setw(10) << it->count << std::endl;
            }

        } else if((line.compare("clear stats")) == 0){
            this->data.clearData();
        } else if((line.compare("auto stats on")) == 0){
            if(this->autoStatTimer == NULL){
                this->autoStatTimer = new EpollTimer(this->data, 2000, epollInstance);
            }
        } else if((line.compare("auto stats off")) == 0){
            if(this->autoStatTimer != NULL){
                delete this->autoStatTimer;
                this->autoStatTimer = NULL;
            }
        } else if((line.compare("exit")) == 0){
            exit(0);

        } else {
            std::cout << "device monitor" << std::endl;
            std::cout << "help             : Prints this help output" << std::endl;
            std::cout << "print stats      : Prints this current statistics" << std::endl;
            std::cout << "clear stats      : Clears this current statistics" << std::endl;
            std::cout << "auto stats on/off: Enables automatic stats" << std::endl;
            std::cout << "exit             : Exit" << std::endl;
        }
    }
}
