#ifndef DEVICEDATA_H
#define DEVICEDATA_H
#include <vector>

typedef struct entry {
    int deviceId;
    int count;
} entry_t;

class DeviceData
{
public:
    DeviceData();
    ~DeviceData();
    int pushData(int devId);
    int clearData(void);
    std::vector<entry_t> getDataCopy(void);
private:
    std::vector<entry_t> devData;
};

#endif // DEVICEDATA_H
