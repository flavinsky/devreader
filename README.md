# Simple Device Reader
This repository holds a small project featuring a server which can monitor multiple devices. If a device is created, it tries to connect to the server and pushes its messages in periodic intervals.  The server can be configured to automatically deliver the statistics of the received messages.

## Preconditions
* The server uses epoll
* Clients use pthreads
* cmake for the compilation

## Initialization
`git submodule update --init`

## Compilation
In the root directory just call:

`make all`

Then the executables device and device_server can be found in the build directory.

## Communication
The devices send their messages through normal TCP/IP sockets to the server. Each message is protected with a CRC16 checksum to ensure data integrity. If the checksum was correct, the server answers with an ACK message to the device. Otherwise a NACK message is sent.

Typical message:

|  Length | Device Id | Msg Id   | Data            | CRC16   |
|:-------:|-----------|----------|-----------------|---------|
| 2 bytes | 4 bytes  | 4 bytes | variable length | 2 bytes |

Standard messages sent from devices to server hold some dummy data in the Data field, whereas this field is actually used in the ACK/NACK messages.

**Important to mention**: Changes in endianness have not been considered yet since data stays on the same platform.

**Important to mention2**: Since the server runs only with one thread at the moment, the internal datastructure is not implemented in a thread safe way. If more threads would be used to serve the epoll instance this needs to be improved for example with a mutex or other mechanism.

## Running the project
Help output of the device_server:
```
./device_server -h
./device_server: invalid option -- 'h'
Usage: ./device_server [options]
Options:
  -p Port
```

Help output of the device:
```
./device -h
./device: invalid option -- 'h'
Usage: ./device [options]
Options:
  -d Delay between messages
  -p Port
  -a Server IP
  -i Device ID
```

To run a simple example launch the server to listen on port 50000:

`./device_server -p 50000`

If you type `help` you should get the following output:
```
help
device monitor
help             : Prints this help output
print stats      : Prints this current statistics
clear stats      : Clears this current statistics
auto stats on/off: Enables automatic stats
exit             : Exit
```
Now lets enable automatic stat delivery:

`auto stats on`

After we have started the server we open two new terminal windows in the build folder to start the devices with the commands:
```
./device -d 1000 -p 50000 -i 42
./device -d 500 -p 50000 -i 12
```

After this has been performed the received messages should be shown by the server.
Have fun ;-)
